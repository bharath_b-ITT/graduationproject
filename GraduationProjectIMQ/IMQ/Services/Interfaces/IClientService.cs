using System;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;

namespace IMQ.Services
{
    public interface IClientService
    {
        public void sendResponseToClient(NetworkStream stream,Response response);
        public void authentication (NetworkStream stream,Request newLoginRequest);
        public void createClient (NetworkStream stream,Request request);
        public void getTopics (NetworkStream stream);
    
    }
}
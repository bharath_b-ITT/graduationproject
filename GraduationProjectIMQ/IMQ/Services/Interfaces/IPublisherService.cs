using System;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;
namespace IMQ.Services
{
    public interface IPublisherService
    {

        public void sendResponseToClient(NetworkStream stream, Response response);
        public void publishMessage(NetworkStream stream, Request request);
        public void createNewTopic(NetworkStream stream, Request request);
        public void handleDeadMessages(NetworkStream stream, Request request);


    }
}
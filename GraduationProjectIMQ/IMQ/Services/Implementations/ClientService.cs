using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;
using Newtonsoft.Json;
using IMQ.Exceptions;
namespace IMQ.Services
{
    public class ClientService : IClientService
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IClientRepository clientRepository;
        private IPublisherService publisherService;
        private ISubscriberService subscriberService;
        private ServerConstants serverConstants;

        public ClientService(IClientRepository clientRepository, IPublisherService publisherService, ISubscriberService subscriberService)
        {
            serverConstants = new ServerConstants();
            this.clientRepository = clientRepository;
            this.publisherService = publisherService;
            this.subscriberService = subscriberService;
            serverConstants = new ServerConstants();
        }
        public void getTopics(NetworkStream stream)
        {
            try
            {
                Response response = new Response();
                Queue<string> topics = this.getAllTopics();
                response.data = topics;
                sendResponseToClient(stream, response);
            }
            catch (FailedSendingTopicsException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }
        public Queue<string> getAllTopics()
        {
            try
            {
                Queue<string> topics = clientRepository.getAllTopics();
                return topics;
            }
            catch
            {
                Console.WriteLine("\n Not able to get topics \n");
                throw new Exception();
            }
        }
        public void createClient(NetworkStream stream, Request request)
        {
            try
            {
                Response response = new Response();
                string role = request.role;
                string clientName = request.clientName;
                string password = request.password;
                clientRepository.createClient(clientName, role, password);
                response.responseMessage = serverConstants.registeredSuccessfully;
                sendResponseToClient(stream, response);
            }
            catch (FailedCreatingClientException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }
        public void authentication(NetworkStream stream, Request newLoginRequest)
        {
            try
            {
                string clientName = newLoginRequest.clientName;
                string role = newLoginRequest.role;
                string password = newLoginRequest.password;
                bool clientVerified = this.authenticateClient(clientName, role, password);
                Response response = new Response();
                response.responseMessage = clientVerified == true ? "true" : "false";
                sendResponseToClient(stream, response);
            }
            catch (FailedAuthenticatingException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }
        public bool authenticateClient(string clientName, string role, string password)
        {
            try
            {
                bool clientVerified = clientRepository.verifyClientCredentials(clientName, role, password);
                return clientVerified;
            }
            catch
            {
                Console.WriteLine("\n Not able to verify client credentials \n");
                throw new Exception();
            }
        }

        public void sendResponseToClient(NetworkStream stream, Response response)
        {
            try
            {
                streamWriter = new StreamWriter(stream);
                streamWriter.WriteLine(JsonConvert.SerializeObject(response));
                streamWriter.Flush();
            }
            catch
            {
                Console.WriteLine("\n Not able to send response to client \n");
                throw new Exception();
            }

        }


    }
}
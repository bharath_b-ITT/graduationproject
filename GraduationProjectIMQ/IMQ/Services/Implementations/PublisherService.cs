using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;
using Newtonsoft.Json;
using IMQ.Exceptions;
namespace IMQ.Services
{
    public class PublisherService : IPublisherService
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IPublisherRepository publisherRepository;
        private ServerConstants serverConstants;
        public PublisherService(IPublisherRepository publisherRepository)
        {
            serverConstants = new ServerConstants();
            this.publisherRepository = publisherRepository;
        }
        public void publishMessage(NetworkStream stream, Request request)
        {
            try
            {
                Response response = new Response();
                string topic = request.topic;
                string clientMessage = request.message;
                this.savePublisherMessage(topic, clientMessage);
                response.responseMessage = serverConstants.publishedMessage;
                sendResponseToClient(stream, response);
            }
            catch (FailedPublishingMessageException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }

        public void savePublisherMessage(string topic, string clientMessage)
        {
            try
            {
                publisherRepository.savePublisherMessage(topic, clientMessage);
            }
            catch
            {
                Console.WriteLine("\n Not able to save message \n");
            }
        }
        public void handleDeadMessages(NetworkStream stream, Request request)
        {
            try
            {
                Response response = new Response();
                string topic = request.topic;
                publisherRepository.handleDeadMessages(topic);
            }
            catch (FailedToHandleDeadMessageException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }
        public void createNewTopic(NetworkStream stream, Request request)
        {
            try
            {
                Response response = new Response();
                string topicName = request.topic;
                this.createTopic(topicName);
                response.responseMessage = serverConstants.createdTopic;
                sendResponseToClient(stream, response);
            }
            catch (FailedToCreateTopicException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }
        public void createTopic(string topicName)
        {
            try
            {
                publisherRepository.createTopic(topicName);
            }
            catch
            {
                Console.WriteLine("\n Not able to create topic  \n");
            }
        }
        public void sendResponseToClient(NetworkStream stream, Response response)
        {
            try
            {
                streamWriter = new StreamWriter(stream);
                streamWriter.WriteLine(JsonConvert.SerializeObject(response));
                streamWriter.Flush();
            }
            catch
            {
                Console.WriteLine("\n Not able to send response to client \n");
                throw new Exception();
            }
        }
    }
}
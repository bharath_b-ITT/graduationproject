using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using IMQ.Services;
using IMQ.Controllers;
using IMQ.Exceptions;
namespace IMQ.Services
{
    class ServerService : IServerService
    {
        private TcpListener server;
        private TcpClient client;
        private string ipAddress;
        private Int32 portNumber;
        private ClientController clientController;
        private ServerConstants serverConstants;

        public ServerService(IClientController clientController)
        {
            serverConstants = new ServerConstants();
            this.InitilazeStream();
            this.clientController = (ClientController)clientController;
        }
        public void startServer()
        {
            try
            {
                server.Start();
                Console.WriteLine("Server Started.....");
                while (true)
                {
                    Console.WriteLine("How many clients do you want to connect");
                    int numberOfClients = int.Parse(System.Console.ReadLine());
                    for (int count = 0; count < numberOfClients; count++)
                    {
                        client = server.AcceptTcpClient();
                        Thread newThread = new Thread(connectToClient);
                        newThread.Start();
                    }
                }

            }
            catch
            {
                Console.WriteLine("\n Not able to start server \n");
                throw new Exception();
            }
        }
        public void connectToClient()
        {
            try
            {
                Console.WriteLine("Client connected to server");
                clientController.connectToClient(client);
            }
            catch
            {
                Console.WriteLine("\n Not able to connect to client \n");
                throw new Exception();
            }
        }

        public void InitilazeStream()
        {
            try
            {
                this.ipAddress = serverConstants.ipAddress;
                this.portNumber = serverConstants.portNumber;
                IPAddress localAddress = IPAddress.Parse(ipAddress);
                server = new TcpListener(localAddress, portNumber);
            }
            catch
            {
                Console.WriteLine("\n Not able to initialize stream \n");
                throw new Exception();
            }
        }
    }
}
using System.Diagnostics;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;
using Newtonsoft.Json;
using IMQ.Exceptions;
namespace IMQ.Services
{
    public class SubscriberService : ISubscriberService
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private ISubscriberRepository subscriberRepository;
        private ServerConstants serverConstants;

        public SubscriberService(ISubscriberRepository subscriberRepository)
        {
            serverConstants = new ServerConstants();
            this.subscriberRepository = subscriberRepository;
        }

        public void getAllSubscribedTopics(NetworkStream stream, Request request)
        {
            try
            {
                Response response = new Response();
                string clientName = request.clientName;
                Queue<string> topics = this.getSubscribedTopics(clientName);
                response.data = topics;
                sendResponseToClient(stream, response);
            }
            catch (FailedToGetSubscrinbedTopicsException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }
        public Queue<string> getSubscribedTopics(string clientName)
        {
            try
            {
                Queue<string> topics = subscriberRepository.getSubscribedTopics(clientName);
                return topics;
            }
            catch
            {
                Console.WriteLine("\n Not able to get subscribed topics \n");
                throw new Exception();
            }
        }
        public void pullMessagesOfSpecifcTopic(NetworkStream stream, Request request)
        {
            try
            {
                Response response = new Response();
                string topicName = request.topic;
                string client = request.clientName;
                Queue<string> messages = this.pullMessages(topicName, client);
                response.data = messages;
                sendResponseToClient(stream, response);
            }
            catch (FailedToPullMessagesException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }
        public Queue<string> pullMessages(string topicName, string clientName)
        {
            try
            {
                Queue<string> messages = subscriberRepository.pullMessages(topicName, clientName);
                return messages;
            }
            catch
            {
                Console.WriteLine("\n Not able to pull messages \n");
                throw new Exception();
            }
        }
        public void subscribeToNewTopic(NetworkStream stream, Request request)
        {
            try
            {
                Response response = new Response();
                string topicName = request.topic;
                string clientName = request.clientName;
                this.subscribeToTopic(clientName, topicName);
                response.responseMessage = serverConstants.subscribedToTopic;
                sendResponseToClient(stream, response);
            }
            catch (FailedToSubscribeException error)
            {
                System.Console.WriteLine(error.Message);
            }
        }
        public void subscribeToTopic(string clientName, string topicName)
        {
            try
            {
                subscriberRepository.subscribeToTopic(clientName, topicName);
            }
            catch
            {
                Console.WriteLine("\n Not able to subscribe to topic  \n");
            }
        }
        public void sendResponseToClient(NetworkStream stream, Response response)
        {
            try
            {
                streamWriter = new StreamWriter(stream);
                streamWriter.WriteLine(JsonConvert.SerializeObject(response));
                streamWriter.Flush();
            }
            catch
            {
                Console.WriteLine("\n Not able to send response to client \n");
                throw new Exception();
            }
        }
    }
}
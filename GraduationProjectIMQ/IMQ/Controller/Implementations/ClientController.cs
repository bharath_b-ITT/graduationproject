using System.Collections.Immutable;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Net.Sockets;
using IMQ.Models;
using IMQ.Repositories;
using IMQ.Controllers;
using IMQ.Services;
using IMQ.Commands;
using IMQ.Exceptions;

using Newtonsoft.Json;
namespace IMQ.Controllers
{
    public class ClientController : IClientController
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IClientService clientService;
        private IPublisherService publisherService;
        private ISubscriberService subscriberService;
        private ServerConstants serverConstants;
        public ClientController(IClientService clientService, IPublisherService publisherService, ISubscriberService subscriberService)
        {
            serverConstants = new ServerConstants();
            this.clientService = clientService;
            this.publisherService = publisherService;
            this.subscriberService = subscriberService;
        }
        public void connectToClient(TcpClient newClient)
        {
            client = newClient;
            initializeNetworkStreams();
            while (client.Connected)
            {
                try
                {
                    Request newRequest = JsonConvert.DeserializeObject<Request>(streamReader.ReadLine());
                    switch (newRequest.action)
                    {
                        case IMQCommands.getAllTopic:
                            clientService.getTopics(stream);
                            break;
                        case IMQCommands.publishMessage:
                            publisherService.handleDeadMessages(stream, newRequest);
                            publisherService.publishMessage(stream, newRequest);
                            break;
                        case IMQCommands.getSubscribedTopic:
                            subscriberService.getAllSubscribedTopics(stream, newRequest);
                            break;
                        case IMQCommands.createTopic:
                            publisherService.createNewTopic(stream, newRequest);
                            break;
                        case IMQCommands.pullMessages:
                            subscriberService.pullMessagesOfSpecifcTopic(stream, newRequest);
                            break;
                        case IMQCommands.subscribeTopic:
                            subscriberService.subscribeToNewTopic(stream, newRequest);
                            break;
                        case IMQCommands.register:
                            clientService.createClient(stream, newRequest);
                            break;
                        case IMQCommands.authenticate:
                            clientService.authentication(stream, newRequest);
                            break;
                    }
                }
                catch
                {
                    System.Console.WriteLine("Client disconnected");
                }
                finally
                {
                    stream.Flush();
                    streamWriter.Flush();
                }
            }
        }

        public void initializeNetworkStreams()
        {
            try
            {
                stream = client.GetStream();
                streamReader = new StreamReader(stream);
                streamWriter = new StreamWriter(stream);
            }
            catch
            {
                System.Console.WriteLine(" not able to initialize network stream");
            }
        }

    }
}
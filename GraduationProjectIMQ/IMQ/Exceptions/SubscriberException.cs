using System;
namespace IMQ.Exceptions
{
    public class FailedToGetSubscrinbedTopicsException : Exception
    {
        public FailedToGetSubscrinbedTopicsException() : base("Not able to get subscribed topics")
        {
        }
    }

    public class FailedToPullMessagesException : Exception
    {
        public FailedToPullMessagesException() : base("Not able to pull messages")
        {
        }
    }
    public class FailedToSubscribeException : Exception
    {
        public FailedToSubscribeException() : base("Not able to subscribe to topic")
        {
        }
    }


}

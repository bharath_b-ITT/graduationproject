using System;
namespace IMQ.Exceptions
{
    public class FailedPublishingMessageException : Exception
    {
        public FailedPublishingMessageException() : base("Not able to publish message")
        {
        }
    }

    public class FailedToHandleDeadMessageException : Exception
    {
        public FailedToHandleDeadMessageException() : base("Not able to handle dead message")
        {
        }
    }

    public class FailedToCreateTopicException : Exception
    {
        public FailedToCreateTopicException() : base("Not able to create topic")
        {
        }
    }


}

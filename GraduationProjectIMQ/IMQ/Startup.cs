using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IMQ.Services;
using IMQ.Repositories;
using Microsoft.Extensions.Hosting;
using IMQ.Controllers;

namespace IMQ
{
    public class Startup
    {
        private static IConfigurationBuilder _builder { get; set; }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
        .ConfigureServices((_, services) =>
        {
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IServerService, ServerService>();
            services.AddScoped<IPublisherService, PublisherService>();
            services.AddScoped<ISubscriberService, SubscriberService>();
            services.AddScoped<IClientRepository, ClientRepository>();
            services.AddScoped<ISubscriberRepository, SubscriberRepository>();
            services.AddScoped<IPublisherRepository, PublisherRepository>();
            services.AddScoped<IClientController, ClientController>();
            services.AddScoped<IServerController, ServerController>();

        });

        public static void BuildConfig(IConfigurationBuilder builder)
        {
            builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            _builder = builder;
        }
    }
}
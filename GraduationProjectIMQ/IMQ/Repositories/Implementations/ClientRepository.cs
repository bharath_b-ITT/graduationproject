﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using IMQ;
using IMQ.Exceptions;

namespace IMQ.Repositories
{
    public class ClientRepository : IClientRepository
    {
        private SqlConnection DbConnection;
        private ServerConstants serverConstants;
        public ClientRepository()
        {
            serverConstants = new ServerConstants();
            initializeDbConnection();
        }
        public void initializeDbConnection()
        {
            try
            {
                var serverConstants = new ServerConstants();
                string connectionString = serverConstants.connectionString;
                SqlConnection connection = new SqlConnection(connectionString);
                this.DbConnection = connection;
                DbConnection.Open();
            }
            catch
            {
                Console.WriteLine("\n Not able to initialize DB connection\n");
            }
        }
        public bool verifyClientCredentials(string clientName, string role, string password)
        {
            try
            {
                string query = "";
                string clientNameFromDb = "";
                string PasswordfromDb = "";
                query = role == serverConstants.publisher ? ClientDbQueries.verifyPublisher : ClientDbQueries.verifySubscriber;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@name", clientName);
                command.Parameters.AddWithValue("@password", password);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    clientNameFromDb = ((string)reader["Name"]);
                    PasswordfromDb = ((string)reader["Password"]);
                }
                reader.Close();
                bool isValidClient = ((clientNameFromDb == clientName) && (PasswordfromDb == password)) ? true : false;
                return isValidClient;
            }
            catch
            {

                throw new FailedAuthenticatingException();
            }
        }
        public Queue<string> getAllTopics()
        {
            try
            {
                string query = ClientDbQueries.getAllTopics;
                SqlCommand command = new SqlCommand(query, DbConnection);
                SqlDataReader reader = command.ExecuteReader();
                Queue<string> topics = new Queue<string>();
                while (reader.Read())
                {
                    topics.Enqueue((string)reader["TopicName"]);
                }
                reader.Close();
                return topics;
            }
            catch
            {
                throw new FailedSendingTopicsException();
            }
        }

        public void createClient(string clientName, string role, string password)
        {
            try
            {
                string query = "";
                query = role == serverConstants.publisher ? ClientDbQueries.createPublisher : ClientDbQueries.createSubscriber;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@clientName", clientName);
                command.Parameters.AddWithValue("@password", password);
                command.ExecuteNonQuery();
            }
            catch
            {

                throw new FailedCreatingClientException();
            }
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using IMQ;
using IMQ.Exceptions;

namespace IMQ.Repositories
{
    public class SubscriberRepository : ISubscriberRepository
    {
        private SqlConnection DbConnection;
        private ServerConstants serverConstants;
        public SubscriberRepository()
        {
            serverConstants = new ServerConstants();
            initializeDbConnection();
        }

        public void initializeDbConnection()
        {
            try
            {
                var serverConstants = new ServerConstants();
                string connectionString = serverConstants.connectionString;
                SqlConnection connection = new SqlConnection(connectionString);
                this.DbConnection = connection;
                DbConnection.Open();
            }
            catch
            {
                Console.WriteLine("\n Not able to initialize  db connection \n");

            }
        }
        private int getTopicId(string topic)
        {
            try
            {
                string query = SubscriberDbQueries.getTopicId;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topic", topic);
                SqlDataReader reader = command.ExecuteReader();
                int topicId = 0;
                while (reader.Read())
                {
                    topicId = ((int)reader["Id"]);
                }
                reader.Close();
                return topicId;
            }
            catch
            {
                Console.WriteLine("\n Not able get topic id from db\n");
                throw new Exception();
            }
        }
        private int getSubscriberId(string name)
        {
            try
            {
                string query = SubscriberDbQueries.getSubscriberId;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@name", name);
                SqlDataReader reader = command.ExecuteReader();
                int subscriberId = 0;
                while (reader.Read())
                {
                    subscriberId = ((int)reader["Id"]);
                }
                reader.Close();
                return subscriberId;
            }
            catch
            {
                Console.WriteLine("\n Not able get subscriber id from db\n");
                throw new Exception();
            }
        }
        public Queue<int> getTopicIdsFromSubscriberTopicMappingTable(int subscriberId)
        {
            try
            {
                string query = SubscriberDbQueries.getTopicIdsFromSubscriberTopicMappingTable;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@subscriberId", subscriberId);
                SqlDataReader reader = command.ExecuteReader();
                Queue<int> topicsIds = new Queue<int>();
                while (reader.Read())
                {
                    topicsIds.Enqueue((int)reader["TopicId"]);
                }
                reader.Close();
                return topicsIds;
            }
            catch
            {
                Console.WriteLine("\n Not able get topic ids from subscriber topic mappping table  from db\n");
                throw new Exception();
            }
        }
        public Queue<string> getSubscribedTopics(string clientName)
        {
            try
            {
                int subscriberId = getSubscriberId(clientName);
                Queue<int> topicsIds = getTopicIdsFromSubscriberTopicMappingTable(subscriberId);
                Queue<string> topics = new Queue<string>();
                foreach (int topicId in topicsIds)
                {
                    string query = SubscriberDbQueries.getSubscribedTopics;
                    SqlCommand command = new SqlCommand(query, DbConnection);
                    command.Parameters.AddWithValue("@topicId", topicId);
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        topics.Enqueue((string)reader["TopicName"]);
                    }
                    reader.Close();
                }
                return topics;
            }
            catch
            {
                throw new FailedToGetSubscrinbedTopicsException();
            }
        }

        public Queue<string> pullMessages(string topic, string clientName)
        {
            try
            {
                int topicId = getTopicId(topic);
                string query = SubscriberDbQueries.pullMessages;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topicId", topicId);
                SqlDataReader reader = command.ExecuteReader();
                Queue<string> messages = new Queue<string>();
                while (reader.Read())
                {
                    messages.Enqueue((string)reader["Message"]);
                }
                reader.Close();
                return messages;
            }
            catch
            {

                throw new FailedToPullMessagesException();
            }
        }

        public void subscribeToTopic(string clientName, string topicName)
        {
            try
            {
                int topicId = getTopicId(topicName);
                int subscriberId = getSubscriberId(clientName);
                string query = SubscriberDbQueries.subscribeToTopic;
                SqlCommand command = new SqlCommand(query, DbConnection);
                command.Parameters.AddWithValue("@topicId", topicId);
                command.Parameters.AddWithValue("@subscriberId", subscriberId);
                command.ExecuteNonQuery();
            }
            catch
            {
                throw new FailedToSubscribeException();

            }
        }
    }
}
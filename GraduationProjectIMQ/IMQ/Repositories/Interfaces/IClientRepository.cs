using System;
using System.Collections.Generic;

namespace IMQ.Repositories
{
    public interface IClientRepository
    {
        void initializeDbConnection();
        bool verifyClientCredentials(string publisherName, string role, string password);
        Queue<string> getAllTopics();
        void createClient(string clientName, string role, string password);


    }
}
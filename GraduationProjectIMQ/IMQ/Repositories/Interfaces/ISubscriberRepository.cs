using System;
using System.Collections.Generic;

namespace IMQ.Repositories
{
    public interface ISubscriberRepository
    {
        void initializeDbConnection();
        void subscribeToTopic(string clientName, string topicName);
        Queue<string> getSubscribedTopics(string clientName);
        Queue<string> pullMessages(string topic, string clientName);
  
}
}
using System;
using System.Data.SqlClient;

namespace IMQ.Repositories
{
    public static class PubisherDbQueris
    {
        public const String createTopic = "INSERT INTO Topics (TopicName) VALUES (@topicName)";
        public const String savePublisherMessage = "INSERT INTO Messages (Message, TopicId, TimeStamp) VALUES (@message, @topicId, @time)";
        public const String getTopicId = "SELECT Id from Topics WHERE TopicName = @topic";
        public const String checkForExpiredMessages = "SELECT Message, TopicId, timeStamp from Messages WHERE TopicId = @topicId and Messages.timeStamp < DATEADD(dd,-9,GETDATE())";
        public const String addDeadMessages = "INSERT INTO DeadLetterMessage (Message, TopicId) VALUES (@message, @topicId)";
        public const String deleteMessages = "DELETE FROM Messages WHERE TopicId = @topicId and Messages.timeStamp < DATEADD(dd,-9,GETDATE())";
    }

}
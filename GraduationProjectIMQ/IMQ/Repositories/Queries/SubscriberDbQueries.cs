using System;
using System.Data.SqlClient;

namespace IMQ.Repositories
{
    public static class SubscriberDbQueries
    {
        public const String subscribeToTopic = "INSERT INTO SubscribedTopicMap (TopicId, SubscriberId) VALUES (@topicId, @subscriberId)";
        public const String pullMessages = "SELECT Message from Messages WHERE TopicId = @topicId";
        public const String getTopicIdsFromSubscriberTopicMappingTable = "SELECT TopicId from SubscribedTopicMap WHERE SubscriberId = @subscriberId";
        public const String getSubscribedTopics = "SELECT TopicName from Topics WHERE Id = @topicId";
        public const String getSubscriberId = "SELECT Id from Subscribers WHERE Name = @name";
        public const String getTopicId = "SELECT Id from Topics WHERE TopicName = @topic";
    }

}
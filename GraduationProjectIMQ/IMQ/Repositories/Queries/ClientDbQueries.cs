using System;
using System.Data.SqlClient;

namespace IMQ.Repositories
{
    public static class ClientDbQueries
    {
        public const String createPublisher = "INSERT INTO Publishers (Name,Password) VALUES (@clientName,@password)";
        public const String createSubscriber = "INSERT INTO Publishers (Name,Password) VALUES (@clientName,@password)";
        public const String getAllTopics = "SELECT * from Topics";
        public const String verifyPublisher = "SELECT Name,Password FROM  Publishers where Name= @name AND Password=@password";
        public const String verifySubscriber = "SELECT Name,Password FROM  Subscribers where Name= @name AND Password=@password";
    }

}
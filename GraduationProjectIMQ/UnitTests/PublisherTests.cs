using System;
using Xunit;
using IMQ.Services;
using Moq;
using IMQ.Models;
using IMQ.Tests;
using IMQ.Repositories;

namespace IMQ.Publisher.Tests
{
    public class PublisherTests
    {
        [Fact]
        public void publishMessage_shouldPass()
        {
            var mockPublisherRepository = new Mock<IPublisherRepository>();
            var request = new Request();
            request.topic = "topicfirst";
            request.message = "tpoicfirstmsgfirst";
            mockPublisherRepository.Setup(repository => repository.savePublisherMessage(request.topic, request.message));
            var pubService = new PublisherService(mockPublisherRepository.Object);
            pubService.savePublisherMessage(request.topic, request.message);
            mockPublisherRepository.Verify(repository => repository.savePublisherMessage(request.topic, request.message), Times.Exactly(1));           
        }

        [Fact]
        public void getAllTopics_shouldPass()
        {
            var mockClientRepository = new Mock<IClientRepository>();
            var mockPublisherService = new Mock<IPublisherService>();
            var mockSubscriberService = new Mock<ISubscriberService>();
            mockClientRepository.Setup(repository => repository.getAllTopics())
            .Returns(TestData.getAllTopics());
            var clientService = new ClientService(mockClientRepository.Object, mockPublisherService.Object, mockSubscriberService.Object);
            var received = clientService.getAllTopics();
            var expected = TestData.getAllTopics();
            Assert.True(received != null);
            mockClientRepository.Verify(repository => repository.getAllTopics(), Times.Exactly(1));
            for(int index=0; index<= received.Count; index++){
                var actualMsg = received.Dequeue();
                var expectedMsg = expected.Dequeue();
                Assert.Equal(actualMsg,expectedMsg);
            }
        }

         [Fact]
        public void createTopic_shouldPass()
        {
            var mockPublisherRepository = new Mock<IPublisherRepository>();
            var request = new Request();
            request.topic = "topicsecond";
            mockPublisherRepository.Setup(repository => repository.createTopic(request.topic));
            var pubService = new PublisherService(mockPublisherRepository.Object);
            pubService.createTopic(request.topic);
            mockPublisherRepository.Verify(repository => repository.createTopic(request.topic), Times.Exactly(1));
        }

         [Theory]
         [InlineData("topicsecond")]
         [InlineData("topicthird")]
         [InlineData("topicfourth")]
        public void createTopicWithTheory_shouldPass(string topic)
        {
            var mockPublisherRepository = new Mock<IPublisherRepository>();
            var request = new Request();
            request.topic = topic;
            mockPublisherRepository.Setup(repository => repository.createTopic(request.topic));
            var pubService = new PublisherService(mockPublisherRepository.Object);
            pubService.createTopic(request.topic);
            mockPublisherRepository.Verify(repository => repository.createTopic(request.topic), Times.Exactly(1));
        }

         [Theory]
         [InlineData("topicsecond","topicsecondmsgsecond")]
         [InlineData("topicthird","topicthirdmsgthird")]
         [InlineData("topicfourth","topicfourthmsgfourth")]
        public void publishMessageWithTheory_shouldPass(string topic, string message)
        {
            var mockPublisherRepository = new Mock<IPublisherRepository>();
            var request = new Request();
            request.topic = topic;
            request.message = message;
            mockPublisherRepository.Setup(repository => repository.savePublisherMessage(request.topic, request.message));
            var pubService = new PublisherService(mockPublisherRepository.Object);
            pubService.savePublisherMessage(request.topic, request.message);
            mockPublisherRepository.Verify(repository => repository.savePublisherMessage(request.topic, request.message), Times.Exactly(1));           
        }
    }
}

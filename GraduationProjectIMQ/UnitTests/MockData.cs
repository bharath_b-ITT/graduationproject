using System.Collections.Generic;
using IMQ.Models;

namespace IMQ.Tests
{
    public static class TestData{

        public static Queue<string> getAllTopics(){

            var testQueue = new Queue<string>();
            var msg1 = "topicfirst";
            var msg2 = "topicsecond";
            testQueue.Enqueue(msg1);
            testQueue.Enqueue(msg2);
            return testQueue;
        }
        public static Queue<string> getSubscribedTopics(){
             var testQueue = new Queue<string>();
            var msg1 = "topicfirst";
            var msg2 = "topicsecond";
            testQueue.Enqueue(msg1);
            testQueue.Enqueue(msg2);
            return testQueue;
        }

        public static Queue<string> getAllSubscriberMessages(){
            var testQueue = new Queue<string>();
            var msg1 = "topicfirstmsgfirst";
            var msg2 = "topicfirstmsgsecond";
            testQueue.Enqueue(msg1);
            testQueue.Enqueue(msg2);
            return testQueue;
        }
    }


}
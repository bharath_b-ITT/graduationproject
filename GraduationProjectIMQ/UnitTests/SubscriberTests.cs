using System;
using Xunit;
using IMQ.Services;
using Moq;
using IMQ.Models;
using IMQ.Tests;
using IMQ.Repositories;

namespace IMQ.Subscriber.Tests
{

    public class SubscriberTests
    {
        [Fact]
        public void getSubscribedTopics_shouldPass()
        {
            var mockSubscriberRepository = new Mock<ISubscriberRepository>();
            var request = new Request();
            request.clientName = "testsubscriber";
            mockSubscriberRepository.Setup(repository => repository.getSubscribedTopics(request.clientName))
            .Returns(TestData.getSubscribedTopics());
            var subService = new SubscriberService(mockSubscriberRepository.Object);
            var received = subService.getSubscribedTopics(request.clientName);
            var expected = TestData.getSubscribedTopics();
            Assert.True(received != null);
           mockSubscriberRepository.Verify(repository => repository.getSubscribedTopics(request.clientName), Times.Exactly(1));
            for(int index=0; index<= received.Count; index++){
                var actualMsg = received.Dequeue();
                var expectedMsg = expected.Dequeue();
                Assert.Equal(actualMsg,expectedMsg);
            }
        }

        [Fact]
        public void pullSubscribedMessages_shouldPass()
        {
            var mockSubscriberRepository = new Mock<ISubscriberRepository>();
            var request = new Request();
            request.clientName = "testsubscriber";
            request.topic = "topicfirst";
            mockSubscriberRepository.Setup(repository => repository.pullMessages(request.topic,request.clientName))
            .Returns(TestData.getAllSubscriberMessages());
            var subService = new SubscriberService(mockSubscriberRepository.Object);
            var received = subService.pullMessages(request.topic,request.clientName);
            var expected = TestData.getAllSubscriberMessages();
            Assert.True(received != null);
            mockSubscriberRepository.Verify(repository => repository.pullMessages(request.topic,request.clientName), Times.Exactly(1));
            for(int index=0; index<= received.Count; index++){
                var actualMsg = received.Dequeue();
                var expectedMsg = expected.Dequeue();
                Assert.Equal(actualMsg,expectedMsg);
            }
        }

         [Fact]
        public void subscribeToTopic_ShouldPass()
        {
            var mockSubscriberRepository = new Mock<ISubscriberRepository>();
            var request = new Request();
            request.clientName = "testsubscriber";
            request.topic = "topicsecond";
            mockSubscriberRepository.Setup(repository => repository.subscribeToTopic(request.clientName, request.topic));
            var subService = new SubscriberService(mockSubscriberRepository.Object);
            subService.subscribeToTopic(request.clientName, request.topic);
            mockSubscriberRepository.Verify(repository => repository.subscribeToTopic(request.clientName, request.topic), Times.Exactly(1));
        }
    }
}

using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Client
{
    public class ClientConstants
    {
        public ClientConstants()
        {
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            IConfiguration config = builder.Build();
            this.ipAddress = config["ServerAddress:IPAddress"];
            this.portNumber = int.Parse(config["ServerAddress:Port"]);
            this.getAllTopic = config["actions:getAllTopic"];
            this.publishMessage = config["actions:publishMessage"];
            this.getSubscribedTopic = config["actions:getSubscribedTopic"];
            this.pullMessages = config["actions:pullMessages"];
            this.subscribeTopic = config["actions:subscribeTopic"];
            this.createTopic = config["actions:createTopic"];
            this.register = config["actions:register"];
            this.authenticate = config["actions:authenticate"];
            this.publisher = config["roles:publisher"];
            this.subscriber = config["roles:subscriber"];
        }
        public String ipAddress;
        public Int32 portNumber;
        public string registerMessage;
        public string getAllTopic;
        public string publishMessage;
        public string getSubscribedTopic;
        public string createTopic;
        public string pullMessages;
        public string subscribeTopic;
        public string register;
        public string authenticate;
        public string publisher;
        public string subscriber;

    }
}
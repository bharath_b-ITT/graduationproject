using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using IMQ.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class ClientService
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IConfiguration config;

        private ClientConstants clientConstants;
        public ClientService()
        {

            clientConstants = new ClientConstants();
        }
        public Queue<string> getAllTopics(NetworkHandler networkHandler)
        {
            sendMessageForGettingAllTopics(networkHandler);
            Queue<string> receivedTopics = receiveTopicsFromServer(networkHandler);
            return receivedTopics;
        }
        public void sendMessageForGettingAllTopics(NetworkHandler networkHandler)
        {
            Request request = new Request();
            request.action = clientConstants.getAllTopic;
            networkHandler.writeToServer(request);
        }

        public Queue<string> receiveTopicsFromServer(NetworkHandler networkHandler)
        {
            Response response = networkHandler.readFromServer();
            Queue<string> receivedtopics = response.data;
            return receivedtopics;
        }
        public bool checkClientAuthentication(NetworkHandler networkHandler, string clientName, string role, string password)
        {
            try
            {
                Request authenticationRequest = new Request();
                authenticationRequest.clientName = clientName;
                authenticationRequest.role = role;
                authenticationRequest.password = password;
                authenticationRequest.action = clientConstants.authenticate;
                networkHandler.writeToServer(authenticationRequest);
                bool isValidClient = isClientAuthenticated(networkHandler);
                return isValidClient;
            }
            catch
            {

                Console.WriteLine("Not able to authenticate Client");
                throw new Exception();
            }
        }

        public bool isClientAuthenticated(NetworkHandler networkHandler)
        {
            Response response = networkHandler.readFromServer();
            string serverResponse = response.responseMessage;
            Console.WriteLine("\nResponse from server: {0}\n");
            if (serverResponse == "true")
            {
                System.Console.WriteLine("Login Successful\n");
                return true;
            }
            else
            {
                System.Console.WriteLine("Login Failed\n");
                return false;
            }
        }

    }
}
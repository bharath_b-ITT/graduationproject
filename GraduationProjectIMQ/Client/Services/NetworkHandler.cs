using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using IMQ.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class NetworkHandler
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IConfiguration config;
        private ClientConstants clientConstants;
        public NetworkHandler()
        {
            clientConstants = new ClientConstants();
        }

        public TcpClient intiliazeClientNetwork()
        {
            string ipAddress = clientConstants.ipAddress;
            Int32 portNumber = clientConstants.portNumber;
            client = new TcpClient(ipAddress, portNumber);
            return client;
        }

        public void createNetworkStreams()
        {
            try
            {
                stream = client.GetStream();
                streamReader = new StreamReader(stream);
                streamWriter = new StreamWriter(stream);
            }
            catch
            {
                Console.WriteLine("Not able to Initialize Network stream");
            }
        }

        public void closeConnection()
        {
            try
            {
                stream.Close();
                client.Close();
            }
            catch
            {
                Console.WriteLine("Not able to close connection");
            }
        }

        public Response readFromServer()
        {
            try
            {
                Response response = JsonConvert.DeserializeObject<Response>(streamReader.ReadLine());
                return response;
            }
            catch
            {
                Console.WriteLine("Not able to read from the server");
                throw new Exception();
            }
        }

        public void writeToServer(Request request)
        {
            try
            {
                streamWriter.WriteLine(JsonConvert.SerializeObject(request));
                streamWriter.Flush();
            }
            catch
            {
                Console.WriteLine("Not able to write to server");
                throw new Exception();
            }
        }
    }
}
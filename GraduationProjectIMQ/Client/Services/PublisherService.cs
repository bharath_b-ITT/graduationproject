using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using IMQ.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{
    class PublisherService
    {
        private TcpClient client;
        private NetworkStream stream;
        private StreamReader streamReader;
        private StreamWriter streamWriter;
        private IConfiguration config;
        private ClientConstants clientConstants;
        private IMQCommandsService imqCommandsService;

        public PublisherService()
        {
            clientConstants = new ClientConstants();
            imqCommandsService = new IMQCommandsService();
        }
        public void publishMessage(NetworkHandler networkHandler)
        {
            string userCommand = getUserInput();
            IMQCommandStructure commandDetails = imqCommandsService.getIMQCommandDetails(userCommand);
            string topic = commandDetails.topic;
            string message = commandDetails.message;
            sendMessageToServer(networkHandler, topic, message);
            receiveResponseFromServer(networkHandler);
        }

        public void sendMessageToServer(NetworkHandler networkHandler, string topic, string message)
        {
            Request request = new Request();
            request.action = clientConstants.publishMessage;
            request.message = message;
            request.topic = topic;
            networkHandler.writeToServer(request);
        }

        public void receiveResponseFromServer(NetworkHandler networkHandler)
        {
            Response response = networkHandler.readFromServer();
            string receivedMessage = response.responseMessage;
            Console.WriteLine("\nResponse From Server: {0}\n", receivedMessage);
        }

        public void createTopic(NetworkHandler networkHandler)
        {
            string userCommand = getUserInput();
            IMQCommandStructure commandDetails = imqCommandsService.getIMQCommandDetails(userCommand);
            string topic = commandDetails.topic;
            sendTopicNameToServer(networkHandler, topic);
            receiveResponseFromServer(networkHandler);

        }

        public void sendTopicNameToServer(NetworkHandler networkHandler, string topic)
        {
            Request request = new Request();
            request.action = clientConstants.createTopic;
            request.topic = topic;
            networkHandler.writeToServer(request);
        }

        public void createClient(NetworkHandler networkHandler)
        {
            System.Console.WriteLine("Register ");
            System.Console.WriteLine("Please Enter Name ");
            var clientName = System.Console.ReadLine();
            System.Console.WriteLine("Please Enter role ");
            var role = System.Console.ReadLine();
            System.Console.WriteLine("Please Create Password ");
            var password = System.Console.ReadLine();
            sendClientDetailsToServer(networkHandler, clientName, role, password);
            receiveResponseFromServer(networkHandler);
        }

        public void sendClientDetailsToServer(NetworkHandler networkHandler, string clientName, string role, string password)
        {
            Request request = new Request();
            request.clientName = clientName;
            request.role = role;
            request.password = password;
            request.action = clientConstants.register;
            networkHandler.writeToServer(request);
        }


        public string getUserInput()
        {
            Console.WriteLine("Enter the IMQ Command ");
            string command = Console.ReadLine();
            return command;
        }

    }
}
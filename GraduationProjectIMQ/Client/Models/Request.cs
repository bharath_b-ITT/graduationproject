using System;
namespace IMQ.Models
{
    public class Request
    {
        private string _topic;
        private string _message;
        public string _clientName;
        private string _action;
        private string _role;
        private string _password;
        public string topic
        {
            get { return _topic; }
            set { _topic = value; }
        }
        public string message
        {
            get { return _message; }
            set { _message = value; }
        }
        public string clientName
        {
            get { return _clientName; }
            set { _clientName = value; }
        }
        public string action
        {
            get { return _action; }
            set { _action = value; }
        }
        public string role
        {
            get { return _role; }
            set { _role = value; }
        }
        public string password
        {
            get { return _password; }
            set { _password = value; }
        }

    }
}

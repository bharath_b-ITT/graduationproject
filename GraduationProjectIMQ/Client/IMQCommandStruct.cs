using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using IMQ.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace Client
{

    public struct IMQCommandStructure
    {
        public string topic;
        public string message;
    }


}